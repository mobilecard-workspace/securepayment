package com.addcel.securepayment.spring.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.securepayment.spring.services.PayworksService;


@RestController
public class PayworksController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PayworksController.class);
	
	private final static String PATH_SECURE_PAYMENT = "{idApp}/{idPais}/{idioma}/payment";
	
	private static final String PATH_TREED_SECURE_RESP = "/3d/secure/response";
	
	private static final String PATH_PAYWORKS_RESP = "/payworks/response";
	
	@Autowired
	PayworksService PService;
	
	/**
	 * Realizar pagos
	 * @param payment
	 * @return
	 */
	@RequestMapping(value = PATH_SECURE_PAYMENT,method = RequestMethod.POST) 
	public ModelAndView SecurePayment(@PathVariable Integer idApp,@PathVariable Integer idPais,@PathVariable String idioma,
			@RequestParam String concept, @RequestParam long idUser,@RequestParam long idCard, @RequestParam double amount, HttpServletRequest request){
		LOGGER.debug("Ejecutando pago..");
		
		return PService.Send3DS(idApp, idPais, idioma, concept, idUser, idCard, amount);
	}
	
	@RequestMapping(value = PATH_TREED_SECURE_RESP)
	public ModelAndView payworksRec3DRespuesta(@RequestBody String cadena) {
		return new ModelAndView();
	}
	
	@RequestMapping(value = PATH_PAYWORKS_RESP, method = RequestMethod.GET)
	public ModelAndView payworks2RecRespuesta(
			@RequestParam String NUMERO_CONTROL,
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam(required = false) String CODIGO_PAYW, @RequestParam(required = false) String RESULTADO_AUT,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW, @RequestParam(required = false) String BANCO_EMISOR,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION, @RequestParam(required = false) String TIPO_TARJETA, @RequestParam(required = false) String MARCA_TARJETA, 
			ModelMap modelo,HttpServletRequest request) {
		return new ModelAndView();
	}
	
	@RequestMapping(value="payworks/error_previo_pago", method = RequestMethod.POST)
	public ModelAndView error_prev(ModelMap modelo, @RequestParam String json){
		ModelAndView view = new ModelAndView("payworks/error_previo_pago");
		LOGGER.debug("[redireecionando] " + json);
		view.addObject("json", json);
		return view;
	}
	
	
}
