package com.addcel.securepayment.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.securepayment.bean.AfiliacionVO;
import com.addcel.securepayment.bean.Card;
import com.addcel.securepayment.bean.TBitacora;


public interface ServiceMapper {
	
	public Card getCard(@Param(value = "idUsuario") long idUsuario, @Param(value = "idTarjeta") long idTarjeta, @Param(value = "idApp") int idApp);
	
	public void addBitacora(TBitacora bitacora);
	
	public AfiliacionVO buscaAfiliacion(@Param(value = "banco")String banco);
	
	public TBitacora getTBitacora(@Param(value = "idBitacora") long idBitacora);
	
}
