package com.addcel.securepayment.bean;

import org.springframework.core.env.Environment;

public class AppProperties {
	
	private Environment env;
	
	public AppProperties(Environment env) {
		this.env = env;
	}
	
	public void setEnv(Environment env) {
		this.env = env;
	}
	
	public Environment getEnv() {
		return env;
	}
	
	public String getProperty(String key){
		return env.getProperty(key);
	}

}
